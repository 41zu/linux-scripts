#!/usr/lib/jvm/default/bin/java --source 21

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * replaces all tab characters in a file
 *
 * @since 2024-05-16
 */
public class TabReplacer {

    static String SPACES = new StringBuilder().repeat(' ', 4).toString();

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Pass text files as arguments, in which tabs should be replaced.");
            return;
        }

        for (String file : args)
            processFile(Path.of(file));
    }

    private static void processFile(Path file) {
        try {
            String fileContends = Files.readString(file, StandardCharsets.UTF_8);

            long tabCount = fileContends.chars()
                .filter(c -> c == '\t')
                .count();

            String processedContents = fileContends.replace("\t", SPACES);
            Files.writeString(file, processedContents, StandardCharsets.UTF_8);

            System.out.println(file + ": " + tabCount + " replaced");
        } catch (Exception e) {
            System.out.println("File " + file + " could not be processed. Error: " + e.toString());
        }
    }
}
